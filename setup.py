#!/usr/bin/python3

from os import listdir, path
from distutils.core import setup
from DistUtilsExtra.command import *
import sys
import subprocess

dist = setup(name='gvb',
      version='1.4',
      description='Good ViBrations',
      license='GPL',
      author='Pietro Battiston',
      author_email='me@pietrobattiston.it',
      url='http://www.pietrobattiston.it/gvb',
      scripts=['gvb'],
      packages=['gvbmod'],
      data_files=[('share/gvb/stuff', ['stuff/logo.svg', 'stuff/wlogo.svg', 'stuff/gvb.glade']),
#                  ('share/doc/gvb', ['README']),
                  ('share/pixmaps', ['stuff/gvb.svg']),
                  ('share/applications', ['stuff/gvb.desktop'])]+
                  [('share/locale/'+lang+'/LC_MESSAGES/', ['locale/'+lang+'/LC_MESSAGES/gvb.mo'] ) for lang in listdir('locale')],
      cmdclass = { "build" :  build_extra.build_extra,
                   "build_help" :  build_help.build_help},
      classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: X11 Applications :: GTK',
        'Intended Audience :: Education',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Topic :: Education :: Testing',
        'Topic :: Scientific/Engineering :: Physics',
        'Topic :: Scientific/Engineering :: Visualization',
        ]    
     )
