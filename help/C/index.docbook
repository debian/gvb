<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "2.18">
<!ENTITY manrevision "2.8">
<!ENTITY date "February 2007">
<!ENTITY app "<application>Good ViBrations</application>">
<!ENTITY appname "Gvb">
]>
<!-- =============Document Header ============================= -->
<article id="index" lang="en_GB">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo> 
    <title>Good ViBrations</title>
    
    <copyright> 
      <year>2009-2014</year> 
      <holder>Pietro Battiston</holder> 
    </copyright>
<!-- translators: uncomment this:

  <copyright>
   <year>2009</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->
<!-- An address can be added to the publisher information.  If a role is 
     not specified, the publisher/author is the same for all versions of the 
     document.  -->
    <publisher role="maintainer"> 
      <publishername>Pietro Battiston (<email>me@pietrobattiston.it</email>)</publishername>
    </publisher> 

      <legalnotice id="legalnotice">
	    <para>This document is made available under the Creative Commons ShareAlike 3.0 License (CC-BY-SA).</para>
	    <para>This documentation is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE AS DESCRIBED IN THE DISCLAIMER.</para>
	    <para>A copy of the licence is available <ulink url="http://creativecommons.org/licenses/by-sa/3.0">here</ulink>.</para>
   </legalnotice>
 
   <!-- This file  contains link to license for the documentation (GNU FDL), and 
        other legal stuff such as "NO WARRANTY" statement. Please do not change 
	any of this. -->
   <authorgroup>
   <author> 
	<firstname>Pietro</firstname> 
	<surname>Battiston</surname> 
	<affiliation> 
	  <orgname>GVB developer</orgname>  
	  <email>me@pietrobattiston.it</email> 
	</affiliation> 
   </author> 
<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
       <othercredit role="translator">
	<firstname>Latin</firstname> 
	<surname>Translator 1</surname> 
	<affiliation> 
	  <orgname>Latin Translation Team</orgname> 
	  <email>translator@gnome.org</email> 
	</affiliation>
	<contrib>Latin translation</contrib>
      </othercredit>
-->
    </authorgroup>
 
    <revhistory>
    <revision> 
		<revnumber>Gvb manual 1.1</revnumber> 
		<date>January 2014</date> 
		<revdescription> 
	  		<para role="author">Pietro Battiston <email>me@pietrobattiston.it</email></para>
	  		<para role="publisher">Pietro Battiston</para>
		</revdescription> 
    </revision> 
    </revhistory> 
    <releaseinfo>This manual describes version 1.3 of Good ViBrations (GVB).</releaseinfo> 
    <legalnotice> 
      <title>Feedback</title> 
      <para>To report a bug or make a suggestion regarding the <application>Good ViBrations</application> application or this manual, please use the project's <ulink url="https://bugs.launchpad.net/gvb" type="help">Launchpad page</ulink>.</para>
<!-- Translators may also add here feedback address for translations -->
    </legalnotice> 
    <abstract role="description">
      <para>User manual for <application>Good ViBrations</application>.</para>
    </abstract>

  </articleinfo> 

  <indexterm>
    <primary>Good ViBrations</primary> 
  </indexterm> 

<!-- ============= Document Body ============================= -->

<!-- ============= Introduction ============================== -->
<sect1 id="introduction">
  <title>Introduction</title>
  <para><application>Good ViBrations</application> is an application that can simulate 1 and 2-dimensional vibrations.</para>

  <sect2 id="using">
    <title>Good ViBrations main window</title>
    <para>The vast majority of <application>Good ViBrations</application> functionalities can be accessed directly from the main screen:</para>
       <figure id="main-window">
		  <title>GVB main window</title>
		  <screenshot>
			 <mediaobject>
				<imageobject>
				  <imagedata fileref="figures/main_window.png" format="PNG"/>
					 </imageobject>
				<textobject> <phrase>Shows the GVB main window.</phrase>
				</textobject>
			</mediaobject>
		  </screenshot>
		</figure>

    <para>On the left, the main action area can be seen, where the simulation is visualized.</para>
    <para>On the right are the various controls and indicators: press <guibutton>Start</guibutton> to see them in action.</para>
    <itemizedlist>
      <listitem>
        <para><guilabel>Speed</guilabel>: sets the speed at which the simulation evolves.</para>
      </listitem>
      <listitem>
        <para><guilabel>Step</guilabel>: in those calculation methods which operate in discrete time (see <xref linkend="calculation-methods"/> for more info), this sets the time between an iteration and the following. Notice that the lower the value, the higher will be the precision of the simulation but also the computational effort required.</para>
      </listitem>
      <listitem>
	<para>Below <guilabel>Step</guilabel>, is the counter, showing how many frames were calculated from the beginning of the simulation.</para>
      </listitem>
      <listitem>
	<para><guilabel>calculation ms.</guilabel> shows the milliseconds which were spent, on average, in calculations for each frame (in the last 2 seconds).</para>
      </listitem>
      <listitem>
	<para><guilabel>drawing ms.</guilabel> shows the milliseconds which were spent, on average, in graphically rendering each frame (in the last 2 seconds).</para>
      </listitem>
      <listitem>
	<para><guilabel>Rope</guilabel> and <guilabel>Membrane</guilabel> allow to switch between (respectively) 1 dimension and 2 dimensions simulations. Changing this will stop and reset the animation.</para>
      </listitem>
      <listitem>
	<para>Below, the button <guibutton>Start</guibutton> now shows <guibutton>Stop</guibutton>: push it in order to stop the animation.</para>
      </listitem>
      <listitem>
        <para><guilabel>Calculation</guilabel>: sets the calculation method used. See <xref linkend="calculation-methods"/> for a list of methods and their descriptions.</para>
      </listitem>
      <listitem>
        <para><guilabel>Frames/second</guilabel>: sets the number of frames shown per second. Under 20, the animation can appear choppy, but over 25, almost no improvement should be noticed (and the computational effort will increase for calculation methods operating in continuous time).</para>
      </listitem>
      <listitem>
        <para><guilabel>Graphics</guilabel>: sets the drawing method used. See <xref linkend="drawing-methods"/> for a list of methods and their descriptions.</para>
      </listitem>
    </itemizedlist>
    <warning>
        <title>Beware excessive precision!</title>
	<para><guilabel>Speed</guilabel>, <guilabel>Step</guilabel> and <guilabel>Frames/second</guilabel> are key parameters (together with the number of points used - see <xref linkend="number-of-points"/>) to determine the number of mathematical operations the computer must perform for each second of simulation: if excessive values are chosen, the animation may become choppy, or <application>Good ViBrations</application> may even freeze, becoming unresponsive. In this case, click <guibutton>Stop</guibutton> (only once): in most cases, the interface will be responsive again in few seconds.</para>
    </warning>
  </sect2>

  <sect2 id="menu-file">
    <title>The <guimenu>File</guimenu> menu</title>
    <para>The <guimenu>File</guimenu> menu lists some general operations:</para>
    <itemizedlist>
      <listitem>
        <para><menuchoice><guimenu>File</guimenu><guimenuitem>Open</guimenuitem></menuchoice>: open a disposition (the definition of points positions and speeds) from a file.</para>
      </listitem>
      <listitem>
        <para><menuchoice><guimenu>File</guimenu><guimenuitem>Save</guimenuitem></menuchoice>: save the disposition and speed of points to a file.</para>
      </listitem>
      <listitem>
        <para><menuchoice><guimenu>File</guimenu><guimenuitem>Save As</guimenuitem></menuchoice>: save the disposition and speed of points to a new file (if you still didn't save any disposition, this is equivalent to <menuchoice><guimenu>File</guimenu><guimenuitem>Save</guimenuitem></menuchoice>).</para>
      </listitem>
      <listitem>
        <para><menuchoice><guimenu>File</guimenu><guimenuitem>Quit</guimenuitem></menuchoice>: exit <application>Good ViBrations</application>.</para>
      </listitem>
    </itemizedlist>
  </sect2>

  <sect2 id="menu-disposition">
    <title>The <guimenu>Disposition</guimenu> menu</title>
    <para>The <guimenu>Disposition</guimenu> menu allows to set the shape of the string or of the membrane at the start of the simulation. This can be done in two ways:</para>
    <itemizedlist>
      <listitem>
        <para>by choosing one of the <guimenuitem>precooked</guimenuitem> dispositions: a collection of them is available, in both 1 and 2 dimensions,</para>
      </listitem>
      <listitem>
        <para>by opening the <guimenuitem>advanced</guimenuitem> editor, also available for both 1 and 2 dimensions (see <xref linkend="advanced-editor"/>).</para>
      </listitem>
    </itemizedlist>
  </sect2>

  <sect2 id="menu-options">
    <title>The <guimenu>Options</guimenu> menu</title>
    <para>The <guimenu>Options</guimenu> menu allows to configure some aspects of the simulation.</para>
    <itemizedlist>
      <listitem>
        <para><menuchoice><guimenu>Options</guimenu><guimenuitem>Number of points</guimenuitem></menuchoice>: change the number of points used in the simulation (see <xref linkend="number-of-points"/>).</para>
      </listitem>
      <listitem>
        <para><menuchoice><guimenu>Options</guimenu><guimenuitem>Save frames as png</guimenuitem></menuchoice>: if this checkbox is toggled, the animation will be saved to disk instead of being rendered to screen: see <xref linkend="dump-to-png"/> for more information.</para>
      </listitem>
    </itemizedlist>
  </sect2>

  <sect2 id="menu-help">
    <title>The <guimenu>Help</guimenu> menu</title>
    <para>The <guimenu>Help</guimenu> menu can be used to access informations about the program.</para>
    <itemizedlist>
      <listitem>
        <para><menuchoice><guimenu>Help</guimenu><guimenuitem>Contents</guimenuitem></menuchoice>: open this help.</para>
      </listitem>
      <listitem>
        <para><menuchoice><guimenu>Help</guimenu><guimenuitem>About</guimenuitem></menuchoice>: show some informations about Good ViBrations.</para>
      </listitem>
    </itemizedlist>
  </sect2>

</sect1>

<!--============== Number of points ===========-->
<sect1 id="number-of-points">
  <title>Changing the number of points</title>
  <para>In <application>Good ViBrations</application>, the string and the membrane are approximated as some points connected by straight lines. The higher the number of points, the higher the quality of the simulation, but also number of computational operations required to perform it. To change the number of points, click <menuchoice><guimenu>Options</guimenu><guimenuitem>Number of points</guimenuitem></menuchoice>.</para>
<warning>
  <para>
    When this dialog is opened, the animation, if running, will automatically pause and, if the number of points is then indeed changed, will reset to its initial disposition, since <application>Good ViBrations</application> is not able to change the number of points of a running simulation.
  </para>
</warning>

  <para>While in 1 dimension any number is equally acceptable, in 2 dimensions the points must be placed on a rectangular (and almost square) grid. If the number chosen does not fit, less points will be used. For instance, if the number of points chosen is 11, only 9 will be used (3×3), if it is 14, only 12 will be used (3×4).</para>

<warning>
<title>When points are too many</title>
  <para>
    The number of points used is a key factor in determining the computational requirements to perform the simulation: performance may deteriorate very fast with their increase in number, and while some number between 100 and 150 should be acceptable on any computer, a number above those values may cause the interface to be unresponsive (depending also on the calculation method chosen - see  <xref linkend="calculation-methods"/> - and on the value of the others parameters explained in <xref linkend="using"/>).
  </para>
</warning>
<tip>
  <para>
     If you want to observe harmonics, some optimal number can be found in the following way:
    <itemizedlist>
      <listitem>
        <para>take a number with many small factors, for instance 2×2×3=12,</para>
      </listitem>
      <listitem>
        <para>substract 1: 11.</para>
      </listitem>
      <listitem>
        <para>if you want to perform a bidimensional simulation, take its square: 11×11=121.</para>
      </listitem>
    </itemizedlist>
     This example will allow you to observe optimally the second, fourth and third harmonics.
  </para>
</tip>

</sect1>

<sect1 id="drawing-methods">
	<title>Drawing methods</title>
	    <para><application>Good ViBrations</application> has three different ways of rendering a simulation, both in 1 and 2 dimensions, accessible through the drop-down box:</para>
        <itemizedlist>
          <listitem>
            <para><guilabel>wave</guilabel>/<guilabel>3D</guilabel> the points are drawn at different heights (in perspective, if working in 2 dimensions), resulting in something indeed similar to a string or to a membrane;</para>
          </listitem>
          <listitem>
            <para><guilabel>speeds</guilabel> is similar to the former, but it also shows the current speed of each point, in the form of a red segment oriented in the direction of movement;</para>
          </listitem>
          <listitem>
            <para><guilabel>geo</guilabel> shows the heights as a geographical maps would show altitudes: points are represented by squares, with colours varying from dark blue (lowest points) to light blue, green, brown, dark brown and finally white (highest points).</para>
          </listitem>
        </itemizedlist>
</sect1>


<!-- ============= Advanced editor ============================== -->
<sect1 id="advanced-editor">
	<title>The advanced dispositions editor</title>
		<para>By clicking <menuchoice><guimenu>Dispositions</guimenu><guimenuitem>1 dimension: advanced</guimenuitem></menuchoice> or <menuchoice><guimenu>Dispositions</guimenu><guimenuitem>2 dimensions: advanced</guimenuitem></menuchoice>, the advanced dispositions editor opens. This is a window that allows to configure the starting position of the string or of the membrane interactively, with much more freedom than by just taking one of the <guimenuitem>precooked</guimenuitem> dispositions.</para>

       <figure id="advanced-editor-2d">
		  <title>The advanced editor in 2 dimensions</title>
		  <screenshot>
			 <mediaobject>
				<imageobject>
				  <imagedata fileref="figures/advanced_editor_2d.png" format="PNG"/>
					 </imageobject>
				<textobject> <phrase>Shows the GVB advanced editor.</phrase>
				</textobject>
			</mediaobject>
		  </screenshot>
		</figure>

		<para>On the top, the <guilabel>Graphics</guilabel> drop-down box makes it possible to change the visualization of the disposition in the editor, similarly to what could be done in the main window. Notice that in this case there is no particular difference between <guilabel>wave</guilabel>/<guilabel>3D</guilabel> and <guilabel>speeds</guilabel>, since there is no speed involved.</para>
		<para>The advanced editor works by tuning the parameters of given wave shapes and combining them. On the left, a preview of the disposition makes it possible to see changes as they are applied. On the right are the various controls that can be used to tune the shape until it is as desired.</para>
<sect2 id="dimension-specific-controls">
    <title>Dimension-speficic controls</title>
    <itemizedlist>
      <listitem>
        <para><guilabel>Waveform</guilabel>: determines the form of the wave to use: can be <guilabel>sinusoidal</guilabel>, <guilabel>triangular</guilabel>, <guilabel>square</guilabel> or just a <guilabel>peak</guilabel>.</para>
      </listitem>
      <listitem>
        <para><guilabel>Total lenght</guilabel>: determines the fraction of the available space on which the new shape be applied. It defaults to all of it, but if for instance the cursor is halfway, the resulting shape will occupy only half of space of the simulation.</para>
      </listitem>
      <listitem>
        <para><guilabel>Shift</guilabel>: determines if the wave shape must start from the first point (default) or from some other point in the middle. Notice that if the wave shape is shifted, any part that overflows from the right/front border will by placed to the left/back, in a sort of rotation.</para>
      </listitem>
      <listitem>
        <para><guilabel>Wavelenght</guilabel>: determines the wavelenght of the wave, which is the space over which its shape repeats, or in other terms the distance between two subsequent peaks of it. It defaults to exactly the whole space, but its value can be tuned down to 4 times less and up to 4 times more.</para>
      </listitem>
      <listitem>
        <para><guilabel>Phase</guilabel>: determines the phase of the wave, which is the offset by which the waveform shifted. If <guilabel>Total lenght</guilabel> is set to the maximum, this has exactly the same effect of <guilabel>Shift</guilabel>; if however this is not the case, it can be used to shift/rotate the wave shape inside the range in which it is applied.</para>
      </listitem>
      <listitem>
        <para><guilabel>Amplitude</guilabel>: determines the amplitude of the wave, which is the vertical distance between its peaks and the rest line: defaults to the maximum, but can be reduced to scale down vertically the wave shape.</para>
      </listitem>
      <listitem>
        <para><guilabel>Centering</guilabel>: determines at which point the waveform must reach its peak: it defaults to the middle - resulting in a symmetric waveform - but can be moved left or right to increase the slope of the shape on the left of the peak and decrease it on the right, or vice versa.</para>
      </listitem>
      <listitem>
        <para><guilabel>Reflect</guilabel>: toggling this check substitutes the wave shape with its symmetric with respect to an horizontal axis in the middle.</para>
      </listitem>
    </itemizedlist>
		<para>Those listed so far are properties that are bound to a single dimension: hence, when working in 2 dimensions, two instances of each are available, one in the <guilabel>Horizontal</guilabel> column and one in the <guilabel>Vertical</guilabel> one, applying respectively to the left-right and to the back-front domain.</para>
</sect2>
<sect2 id="general-controls">
    <title>General controls</title>
		<para>The other options that follow are instead general, applying to 1 or 2 dimensions.</para>
    <itemizedlist>
      <listitem>
        <para><guilabel>Invert</guilabel>: toggling or untoggling this check inverts the wave shape, hence changing the height of every point through a reflection on the horizontal plane: what is above will go below, and vice versa.</para>
      </listitem>
      <listitem>
        <para><guilabel>Composition rule</guilabel> (only enabled when working in 2 dimensions): since the dimension-specific controls only result in the definition of two particular 1-dimensional wave shapes, which will be oriented one from left to right and one from back to front, here it is possible to establish how those two shapes must then be combined:</para>
        <itemizedlist>
          <listitem><guilabel>product</guilabel>: each point's height will be the product of the heights given by the two shapes;
          </listitem>
          <listitem><guilabel>sum</guilabel>: each point's height will be the sum of the heights given by the two shapes;
          </listitem>
          <listitem><guilabel>maximum</guilabel>: each point's height will be the maximum value it takes among the two shapes;
          </listitem>
          <listitem><guilabel>minimum</guilabel>: each point's height will be the minimum value it takes among the two shapes.
          </listitem>
        </itemizedlist>
      </listitem>
      <listitem>
        <para><guilabel>Add to current disposition</guilabel>: if the number of dimensions we are working in is the same as the one currently used in the main window's simulation, it is possible, by toggling this check, to sum the wave form just created to the current state of the simulation. This is a very powerful tool to get arbitrarily complex dispositions, by just adding one wave shape at a time, possibly each restricted to a particular section of the available space.</para>
      </listitem>
    </itemizedlist>

        <para>On the bottom of the window, three buttons are available.</para>
    <itemizedlist>
      <listitem>
        <para><guibutton>Revert</guibutton> resets all the regulations to their default values.</para>
      </listitem>
      <listitem>
        <para><guibutton>Cancel</guibutton> closes the window, discarding any change made.</para>
      </listitem>
      <listitem>
        <para><guibutton>Apply</guibutton> closes the window, applying all changes made.</para>
      </listitem>
    </itemizedlist>        
        
</sect2>
</sect1>
<sect1 id="calculation-methods">
	<title>Calculation methods</title>
	    <para>Several calculation methods are available in <application>Good ViBrations</application>, and are accessible from the <guilabel>Calculation</guilabel> drop-down box in the main window.</para>
    <warning>
        <title>Warning, technical details!</title>
        <para>This page contains a technically detailed list of the algorythms available in <application>Good ViBrations</application>, and as such it is probably not understandable to readers not practical with the mathematical concepts used. Since it's not necessary to be graduated in math to use <application>Good ViBrations</application>, here's a smaller and simpler summary:</para>
        <itemizedlist>
          <listitem>
            <para><guilabel>naif</guilabel>: quite unprecise and slow, no real reason to ever use it;</para>
          </listitem>
          <listitem>
            <para><guilabel>matrix</guilabel>: faster than <guilabel>naif</guilabel>, and much less precise;</para>
          </listitem>
          <listitem>
            <para><guilabel>quad</guilabel>: more precise, but requires more computational power;</para>
          </listitem>
          <listitem>
            <para><guilabel>eig</guilabel>: less precise than <guilabel>quad</guilabel> for very short animations, but the most precise on the long run, since it will not introduce and amplify errors as time passes by: however, it must do some initial calculations, which may take a lot of time and make the program interface become unresponsive before the animation starts, if a high numbers of points is used.</para>
          </listitem>
        </itemizedlist>
    </warning>
        <para>The more detailed list follows.</para>
        <itemizedlist>
          <listitem>
            <para><guilabel>naif</guilabel> operates in discrete time: for each time period, it calculates the acceleration of each point, which is equal to the difference between its previous position and the average previous positions of the surrounding point; then it calculates the speed of each point, which is equal to the previous speed plus the acceleration multiplied by the time elapsed (which is given by the <guilabel>Step</guilabel> parameter), and finally the new position of each point, which is equal to the previous position plus the speed again multiplied by time.</para>
          </listitem>
          <listitem>
            <para><guilabel>matrix</guilabel> is similar to <guilabel>naif</guilabel> except it doesn't calculate the points singularly, but considers them as a vector, which is updated iteration after iteration through the multiplication by a proper matrix. However such vector contains both positions and speeds: the consequence of this is that positions are updated according to the previous speeds. This makes the method much less stable over time (in particular for large values of <guilabel>Step</guilabel>). This said, computationally wise it can be considered as a benchmark: comparing calculation times with <guilabel>naif</guilabel>'s ones gives an idea of how much the underying scipy libraries for matrix computations are able to perform better than native Python code.</para>
          </listitem>
          <listitem>
            <para><guilabel>quad</guilabel> is similar to <guilabel>naif</guilabel>, except it doesn't linearize the calculation of speed, which is no more calculated as the simple difference in height with respect to the surrounding points, but instead takes in consideration the "real" distance, including the horizontal component.</para>
          </listitem>
          <listitem>
            <para><guilabel>eig</guilabel> operates in continuous time: it takes the linearized problem (as <guilabel>naif</guilabel>), but considers the associated system of differential equations, and computes the exponential of the matrix which determine it: for this step to be accomplished, it must calculate the eigenvalues and eigenvectors of it (through the scipy library). Once this calculation is done, it simply multiplies together the resulting matrix and the diagonal matrix given by the exponentials of the eigenvalues.</para>
          </listitem>
        </itemizedlist>
</sect1>
<sect1 id="dump-to-png">
	<title>Saving frames as images</title>
	    <para>When <menuchoice><guimenu>Options</guimenu><guimenuitem>Save frames as png</guimenuitem></menuchoice> is clicked, a dialog will open asking to choose a directory, and the frames will be saved in that directory as png images instead than being rendered to screen. This is useful when they are needed to create a video, or when the complexity of calculation involved is simply to high for them to be seen in real time.</para>
	    <para>Clicking it again will untoggle the checkbox, and the animation will start to be drawn again to screen.</para>
	    <tip>An easy way to transform the produced images into a video is the following:
        <itemizedlist>
          <listitem>
            <para>ensure the program <command>avconv</command> (part of the library <application>libav</application>) is installed on your system,</para>
          </listitem>
          <listitem>
            <para>open a terminal and move to the directory containing the images prepared with <application>Good ViBrations</application>,</para>
          </listitem>
          <listitem>
            <para>give the command <command>avconv -i %08d.png video.mp4</command>.</para>
          </listitem>
        </itemizedlist></tip>
</sect1>
</article>
