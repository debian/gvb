#! /usr/bin/python

import subprocess, os

#PREPARE THE TEMPLATE:
subprocess.call(['xgettext', '-o', 'po/good-vibrations.pot', 'gvb.py', 'gvbmod/dispositions.py', 'stuff/gvb.glade'])

#INITIALIZE:
#msginit --input=messages-all.pot
#mv it.po po/it.po
#mkdir -p trunk/locale/it/LC_MESSAGES/

#UPDATE:

#for strings_file in os.listdir('po'):
#    if not strings_file.endswith('.po'):
#        continue
#    lang = strings_file[:-3]
#    subprocess.call(['msgmerge', '-U', 'po/%s' % strings_file, 'po/good-vibrations.pot'])
#    if not os.path.exists('locale/%s/LC_MESSAGES' % lang):
#        os.mkdir('locale/%s' % lang)
#        os.mkdir('locale/%s/LC_MESSAGES' % lang)
#    subprocess.call(['msgfmt', 'po/%s' % strings_file, '-o', 'locale/%s/LC_MESSAGES/gvb.mo' % lang])

# (commented because now Launchpad does it)
