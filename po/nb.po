# Norwegian Bokmal translation for gvb
# Copyright (c) 2008 Rosetta Contributors and Canonical Ltd 2008
# This file is distributed under the same license as the gvb package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: gvb\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2009-11-14 16:36+0100\n"
"PO-Revision-Date: 2010-01-19 19:37+0000\n"
"Last-Translator: Tor Syversen <sol-moe@online.no>\n"
"Language-Team: Norwegian Bokmal <nb@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2010-02-15 04:46+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#: gvb.py:270
msgid "Stop"
msgstr "Stopp"

#: gvb.py:277 stuff/gvb.glade:393
msgid "Start"
msgstr "Start"

#: gvb.py:309
#, python-format
msgid "%d dimension: precooked"
msgstr ""

#: gvb.py:310
#, python-format
msgid "%d dimension: advanced"
msgstr ""

#: gvb.py:312
#, python-format
msgid "%d dimensions: precooked"
msgstr ""

#: gvb.py:313
#, python-format
msgid "%d dimensions: advanced"
msgstr ""

#: gvbmod/dispositions.py:160
msgid "flat"
msgstr "flat"

#: gvbmod/dispositions.py:160
msgid "sin"
msgstr "sinus"

#: gvbmod/dispositions.py:160
msgid "half sin"
msgstr "halv sinus"

#: gvbmod/dispositions.py:160
msgid "picked"
msgstr ""

#: gvbmod/dispositions.py:160
msgid "triangular signal"
msgstr "trekant signal"

#: gvbmod/dispositions.py:160
msgid "sinusoidal signal"
msgstr "sinusformet signal"

#: gvbmod/dispositions.py:160
msgid "picked lateral"
msgstr ""

#: gvbmod/dispositions.py:160
msgid "opposite triangulars"
msgstr ""

#: gvbmod/dispositions.py:160
msgid "opposite sinusoidals"
msgstr "motsatt sinusformet"

#: gvbmod/dispositions.py:160
msgid "square"
msgstr "firkant"

#: gvbmod/dispositions.py:160
msgid "square signal"
msgstr "firkant signal"

#: gvbmod/dispositions.py:160
msgid "discontinuous peak"
msgstr ""

#: gvbmod/dispositions.py:160
msgid "cos (shifted)"
msgstr ""

#: gvbmod/dispositions.py:160
msgid "pond"
msgstr ""

#: gvbmod/dispositions.py:160
msgid "waterfall"
msgstr "foss"

#: gvbmod/dispositions.py:160
msgid "gut"
msgstr ""

#: gvbmod/dispositions.py:160
msgid "peak"
msgstr ""

#: gvbmod/dispositions.py:160
msgid "sinusoidal"
msgstr "sinusformet"

#: stuff/gvb.glade:23
msgid "_File"
msgstr "_Fil"

#: stuff/gvb.glade:80
msgid "_Disposition"
msgstr "_disposisjon"

#: stuff/gvb.glade:92
msgid "_Options"
msgstr "_Alternativer"

#: stuff/gvb.glade:102 stuff/gvb.glade:1249
msgid "Number of points"
msgstr "Antall poeng"

#: stuff/gvb.glade:111
msgid "Save frames as png"
msgstr "Lagre bilder som png"

#: stuff/gvb.glade:123
msgid "_Help"
msgstr "_Hjelp"

#: stuff/gvb.glade:130
msgid "_Contents"
msgstr "_Innhold"

#: stuff/gvb.glade:195
msgid "Speed"
msgstr "Hastighet"

#: stuff/gvb.glade:231
msgid "Step"
msgstr "Steg"

#: stuff/gvb.glade:294
msgid "calculation ms."
msgstr "kalkulasjon ms."

#: stuff/gvb.glade:329
msgid "drawing ms."
msgstr "tegner ms."

#: stuff/gvb.glade:355
msgid "Rope"
msgstr ""

#: stuff/gvb.glade:371
msgid "Membrane"
msgstr "Membran"

#: stuff/gvb.glade:415
msgid "Calculation"
msgstr "Beregning"

#: stuff/gvb.glade:454
msgid "Frames/second"
msgstr "Bilder/sekund"

#: stuff/gvb.glade:491 stuff/gvb.glade:644
msgid "Graphics"
msgstr "Grafikk"

#: stuff/gvb.glade:565
msgid "About Good ViBrations"
msgstr "Om Good ViBrations"

#: stuff/gvb.glade:572
msgid "A waves simulator."
msgstr "En bølgesimulator"

#: stuff/gvb.glade:574
msgid "Visit GVB website."
msgstr "Besøk GVB web side."

#: stuff/gvb.glade:575
msgid ""
"Good ViBrations is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the Free "
"Software Foundation; either version 3 of the License, or (at your option) "
"any later version.\n"
"\n"
"Good ViBrations is distributed in the hope that it will be useful, but "
"WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY "
"or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for "
"more details.\n"
"\n"
"You should have received a copy of the GNU General Public License along with "
"Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 "
"Temple Place - Suite 330, Boston, MA 02111-1307, USA."
msgstr ""
"Good ViBrations er fri mykvare; du kan redistribuere det og/eller modifisere "
"det under vilkårene i GNU General Public Licence publisert av Free Software "
"Foundation; enten versjon 3 av lisensen, eller (etter eget valg) en senere "
"versjon.\n"
"\n"
"Good ViBrations er disribuert i håp om at det er nyttig. men det kommer UTEN "
"NOEN GARANTI; uten engang en underforstått garanti om SALGBARHET eller "
"DUGELIGHET FOR EN GITT HENSIKT. Se GNU General Public License for flere "
"detaljer.\n"
"\n"
"Du burde ha fått en kopi av GNU General Public License sammen med Good "
"ViBrations; hvis ikke, skriv til Free Software Foundation, Inc., 39 Temple "
"place - Suite 330, Boston, MA 02111-1307, USA."

#: stuff/gvb.glade:620
msgid "Initial position"
msgstr "Utgangsposisjon"

#: stuff/gvb.glade:690
msgid "Add to current disposition"
msgstr ""

#: stuff/gvb.glade:720
msgid "Composition rule:"
msgstr ""

#: stuff/gvb.glade:728
msgid "product"
msgstr "produkt"

#: stuff/gvb.glade:742
msgid "sum"
msgstr "sum"

#: stuff/gvb.glade:757
msgid "maximum"
msgstr "maksimum"

#: stuff/gvb.glade:772
msgid "minimum"
msgstr "minimum"

#: stuff/gvb.glade:796
msgid "Vertical"
msgstr "Loddrett"

#: stuff/gvb.glade:807
msgid "Horizontal"
msgstr "Vannrett"

#: stuff/gvb.glade:1048 stuff/gvb.glade:1064
msgid "Reflect"
msgstr "Reflekter"

#: stuff/gvb.glade:1082
msgid "Waveform"
msgstr "Bølgeform"

#: stuff/gvb.glade:1093
msgid "Total lenght"
msgstr "Total lengde"

#: stuff/gvb.glade:1104
msgid "Shift"
msgstr ""

#: stuff/gvb.glade:1115
msgid "Wavelenght"
msgstr "Bølgelengde"

#: stuff/gvb.glade:1126
msgid "Phase"
msgstr "Fase"

#: stuff/gvb.glade:1137
msgid "Amplitude"
msgstr "Amplitude"

#: stuff/gvb.glade:1148
msgid "Centering"
msgstr "Sentrering"

#: stuff/gvb.glade:1157
msgid "Invert"
msgstr "Inverter"

#: stuff/gvb.glade:1271
msgid "Total number of points:"
msgstr "Totalt antall piksler:"

#: stuff/gvb.glade:1298
msgid ""
"When working in two dimensions, points are arranged in a rectangular grid: "
"if the chosen number doesn't fit, some points may be dropped."
msgstr ""

#: stuff/gvb.glade:1314
msgid ""
"If you want to observe harmonics, the perfect number of points per dimension "
"is an n such that n+1 has a lot of small factors (e.g: if n+1 contains many "
"times 2 as factor, it will be possible to draw better the second harmonic, "
"the  fourth and so on)."
msgstr ""

#: stuff/gvb.glade:1322
msgid "Hint:"
msgstr "Hint:"

#: stuff/gvb.glade:1387
msgid "Choose a path"
msgstr "Velg en bane"

#: stuff/gvb.glade:1454
msgid "File exists"
msgstr "Filen eksisterer"

#: stuff/gvb.glade:1468
msgid ""
"There is already a file with this name,\n"
"are you sure you want to overwrite it?"
msgstr ""
"Det finnes allerede en fil med dette navnet\n"
"er du sikker på at du vil erstatte den?"

#~ msgid "Copyright Pietro Battiston 2008"
#~ msgstr "Copyright Pietro Battiston 2008"
