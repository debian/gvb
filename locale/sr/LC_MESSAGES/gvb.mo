��    2      �  C   <      H     I     `     x     �     �     �  	   �  	   �     �     �                  
   &     1     B     S     Y     l     r     x     ~     �  N   �     �     �     �            
   !  �   ,  	   �     �     �     �     �     �     �     �     �     �          )     1     5     G     N     \     `  �  r  '   /	  7   W	  '   �	  7   �	      �	  &   
     7
     J
     _
  $   
     �
     �
     �
     �
     
     (     >  '   G     o     ~     �     �     �  �   �     H  #   d     �  +   �     �     �    �          #     5     G     S  
   i     t     �     �  )   �  #   �     �     	  %        >     M     m  !   v        
       2   #                 /      %      0   $      &                 *   	       ,                       (                      -                                         +   !   '             .   "             )          1        %d dimension: advanced %d dimension: precooked %d dimensions: advanced %d dimensions: precooked A waves simulator. About Good ViBrations Amplitude Centering Choose a path Composition rule: File exists Frames/second Hint: Horizontal Initial position Number of points Phase Save frames as png Shift Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Total number of points: Vertical Visit GVB website. Waveform Wavelenght When working in two dimensions, points are arranged in a rectangular grid: if the chosen number doesn't fit, some points may be dropped. _Contents _Disposition _File _Help _Options flat half sin maximum minimum opposite sinusoidals opposite triangulars product sin sinusoidal signal square square signal sum triangular signal Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2011-07-18 18:53+0000
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <sr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2011-07-19 04:32+0000
X-Generator: Launchpad (build 13405)
 %d димензија: напредно %d димензија: са већ колачићима %d димензије: напредно %d димензије: са већ колачићима Симулатор таласа. О Добрим ВиБрацијама Амплитуда Центрирање Изаберите путању Правило састављања: Датотека постоји Кадрова/секунди Наговештај: Водоравнo Почетни положај Број тачака Фаза Сачувај кадар као пнг Померај Брзина Покрени Korak Заустави Већ постоји датотека под овим називом,
да ли сте сигурни да желите да је препишете? Укупно трајање Укупан број тачака: Усправно Посетите веб сајт ДВБ-а. Облик таласа Дужина таласа Када се ради у дводимензионалном простору, тачке су распоређене у правоугаоној мрежи: ако изабрани број не може да стане, неке тачке морају бити одбачене. _Садржаји _Распоред _Датотека _Помоћ _Могућности равно полу синусно највише најмање обрнути сиснусоидални обрнути троугласти производ синусно синусоидални сигнал квадрат квадратни сигнал сума троугласти сигнал 