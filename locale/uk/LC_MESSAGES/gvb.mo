��    F      L  a   |                      0     H     a     t     �  	   �     �  	   �     �     �     �     �  �  �     �	     �	  
   �	  �   �	     �
     �
     �
     �
     �
     �
                          &     ,     1  N   6     �     �     �     �     �  
   �  �   �  	   c     m     z     �     �     �     �     �     �     �     �     �     �     �     �                    $     3     8     @  
   D     O     a     h     v     z  	   �  �  �  !   >  :   `  #   �  <   �     �       3   2     f     y     �     �  $   �     �     �  �       �     �     �  �    %   �     �     �     �          #  
   <  1   G     y     �     �     �     �  �   �     E  /   ]     �  )   �     �     �    �            	   5     ?     O     c  !   �     �     �     �  
   �     �          &  /   5  %   e     �     �     �     �     �  
   �     �  )        /  #   >     b     k     �                .                #                      E       +   =   -   (                :          8                  >       3   7          '   2      !   C          "       0      &   )   @       5   /       ,             <   $         D                  %   *                 	   ;   4       9          A       F   
         6       B   ?   1            %d dimension: advanced %d dimension: precooked %d dimensions: advanced %d dimensions: precooked A waves simulator. About Good ViBrations Add to current disposition Amplitude Calculation Centering Choose a path Composition rule: File exists Frames/second Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Graphics Hint: Horizontal If you want to observe harmonics, the perfect number of points per dimension is an n such that n+1 has a lot of small factors (e.g: if n+1 contains many times 2 as factor, it will be possible to draw better the second harmonic, the  fourth and so on). Initial position Invert Membrane Number of points Phase Reflect Rope Save frames as png Shift Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Total number of points: Vertical Visit GVB website. Waveform Wavelenght When working in two dimensions, points are arranged in a rectangular grid: if the chosen number doesn't fit, some points may be dropped. _Contents _Disposition _File _Help _Options calculation ms. cos (shifted) discontinuous peak drawing ms. flat gut half sin maximum minimum opposite sinusoidals opposite triangulars peak picked picked lateral pond product sin sinusoidal sinusoidal signal square square signal sum triangular signal waterfall Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2010-07-02 10:55+0000
Last-Translator: Sergiy Gavrylov <sergiovana@bigmir.net>
Language-Team: Ukrainian <uk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-07-03 03:43+0000
X-Generator: Launchpad (build Unknown)
 %d вимір: розширено %d вимір: попередньо приготовано %d виміри: розширено %d виміри: попередньо приготовано Симулятор хвиль. Про Good ViBrations Додати до чинного положення Амплітуда Обчислення Центрування Вибрати шлях Правило композиції: Файл вже існує Кадрів в секунду Good ViBrations — вільна програма; ви можете розповсюджувати її та/або змінювати за умовами GNU General Public License як опубліковано Free Software Foundation; або версія 3 цієї ліцензії, або (на ваш вибір) пізнішою.

Good ViBrations розповсюджується з надією бути корисною, але БЕЗ БУДЬ-ЯКОЇ ГАРАНТІЇ; навіть без неявної гарантії КОМЕРЦІЙНОЇ СПРОМОЖНОСТІ або ПРИДАТНОСТІ ДЛЯ КОНКРЕТНОЇ МЕТИ. Детальніше дивіться GNU General Public License.

Ви мали отримати копію GNU General Public License разом з Good ViBrations; якщо ні, напишіть за адресою Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Графіки Підказка: Горизонтально Якщо ви хочете бачити гармоніки, точна кількість точок у вимірності буде n тому, що n+1 має багато дрібних чинників (наприклад, якщо n+1 містить багато разів 2, як чинник, можна буде краще малювати другу гармоніку, четверту тощо). Початкове положення Інвертувати Мембрана Кількість точок Фаза Відображення Канат Зберегти кадри у форматі png Зсув Швидкість Запустити Крок Зупинити Файл з такою назвою вже існує, ви
впевнені, що хочете перезаписати його? Повна ширина Загальна кількість точок: Вертикально Відвідайте веб-сайт GVB. Форма хвилі Довжина хвилі Під час роботи в двох вимірах, точки розташовуються в прямокутній ґратці: якщо вибрана кількість не невідповідна, деякі точки можуть бути відкинуті. _Зміст _Розташування _Файл _Довідка _Параметри обчислення (мс.) косинус (зміщений) переривистий пік малювання (мс.) плоский канал половина синуса максимум мінімум протилежні синусоїдальні протилежні трикутні пік вибраний вибраний бічний басейн продукт синус синусоїдальний синусоїдальний сигнал квадрат Прямокутний сигнал сума трикутний сигнал водоспад 