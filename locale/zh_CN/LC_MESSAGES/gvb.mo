��    F      L  a   |                      0     H     a     t     �  	   �     �  	   �     �     �     �     �  �  �     �	     �	  
   �	  �   �	     �
     �
     �
     �
     �
     �
                          &     ,     1  N   6     �     �     �     �     �  
   �  �   �  	   c     m     z     �     �     �     �     �     �     �     �     �     �     �     �                    $     3     8     @  
   D     O     a     h     v     z  	   �  �  �     C     S     c     s     �     �     �     �     �     �     �     �            z       �  	   �     �  �   �     f     s     z     �     �     �     �     �     �     �     �     �     �  4   �     �                    /     6  u   =  
   �  
   �  
   �  
   �  
   �     �     �               +     2  	   9     C     J     Q     a     n     u     |     �     �     �     �     �     �     �     �     �     �                .                #                      E       +   =   -   (                :          8                  >       3   7          '   2      !   C          "       0      &   )   @       5   /       ,             <   $         D                  %   *                 	   ;   4       9          A       F   
         6       B   ?   1            %d dimension: advanced %d dimension: precooked %d dimensions: advanced %d dimensions: precooked A waves simulator. About Good ViBrations Add to current disposition Amplitude Calculation Centering Choose a path Composition rule: File exists Frames/second Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Graphics Hint: Horizontal If you want to observe harmonics, the perfect number of points per dimension is an n such that n+1 has a lot of small factors (e.g: if n+1 contains many times 2 as factor, it will be possible to draw better the second harmonic, the  fourth and so on). Initial position Invert Membrane Number of points Phase Reflect Rope Save frames as png Shift Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Total number of points: Vertical Visit GVB website. Waveform Wavelenght When working in two dimensions, points are arranged in a rectangular grid: if the chosen number doesn't fit, some points may be dropped. _Contents _Disposition _File _Help _Options calculation ms. cos (shifted) discontinuous peak drawing ms. flat gut half sin maximum minimum opposite sinusoidals opposite triangulars peak picked picked lateral pond product sin sinusoidal sinusoidal signal square square signal sum triangular signal waterfall Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2010-12-19 04:18+0000
Last-Translator: ZhangCheng <skyxxzc@gmail.com>
Language-Team: Chinese (Simplified) <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-12-20 04:47+0000
X-Generator: Launchpad (build Unknown)
 %d 维：高级 %d 维：预设 %d 维：高级 %d 维：预设 一款波模拟器。 关于 Good ViBrations 添加到当前的设定 振幅 计算 中心 选择一个路径 合成规则： 文件存在 帧/秒 Good ViBrations 是自由软件；你可以遵照自由软件基金会出版的 GNU 通用公共许可证条款来修改和重新发布这一程序；可采用该许可证的第三版，或者(根据您的选择)用任何更新的版本。

发布 Good ViBrations 的目的是希望它有用，但没有任何担保，甚至没有适合特定目的的隐含的担保。更详细的情况请参阅 GNU 通用公共许可证。

您应该已经和 Good ViBrations 收到一份 GNU 通用公共许可证的拷贝。如果还没有，可写信至： Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 图形 提示： 横向 如果您要观察和音，最好的每维度点数 n 就是那些 n＋1 有很多小因子的数(例如如果 n＋1 有很多因子2，那会画出更好的二次谐波，四次等等)。 初始位置 反转 薄膜 点数 相位 反射 绳子 帧保存为 移位 速度 启动 步 停止 已有一个同名文件，
您真的要重写吗？ 总长 点总数： 纵向 访问 GVB 网站。 波形 波长 在二维下，点会被安排到一个矩形网格中。如果选择的数不合适，一些点可能会被舍弃。 内容(_C) 重设(_D) 文件(_F) 帮助(_H) 选项(_O) 计算(毫秒)： 余弦 (移位) 间断脉冲 绘图(毫秒)： 平展 海峡 半正弦 最大 最小 反向正弦波 反向三角 锋值 尖头 侧部尖头 池塘 乘 正弦 正弦 正弦信号 方波 方波信号 加 三角形信号 瀑布 