��    F      L  a   |                      0     H     a     t     �  	   �     �  	   �     �     �     �     �  �  �     �	     �	  
   �	  �   �	     �
     �
     �
     �
     �
     �
                          &     ,     1  N   6     �     �     �     �     �  
   �  �   �  	   c     m     z     �     �     �     �     �     �     �     �     �     �     �     �                    $     3     8     @  
   D     O     a     h     v     z  	   �  �  �  -   /  3   ]  -   �  3   �     �  %     =   6     t     �     �     �  *   �  $   �         0     =     L     `  �  }  %   :     `     {     �     �     �     �  (   �  
                   /     6  �   K     �  +   �          8     W     m  	  �     �     �  	   �     �     �     �  #   �          5     N  
   ]     h     �     �  ;   �  5   �            !   2     T     a  
   p     {  )   �     �  '   �  
   �  #        (                .                #                      E       +   =   -   (                :          8                  >       3   7          '   2      !   C          "       0      &   )   @       5   /       ,             <   $         D                  %   *                 	   ;   4       9          A       F   
         6       B   ?   1            %d dimension: advanced %d dimension: precooked %d dimensions: advanced %d dimensions: precooked A waves simulator. About Good ViBrations Add to current disposition Amplitude Calculation Centering Choose a path Composition rule: File exists Frames/second Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Graphics Hint: Horizontal If you want to observe harmonics, the perfect number of points per dimension is an n such that n+1 has a lot of small factors (e.g: if n+1 contains many times 2 as factor, it will be possible to draw better the second harmonic, the  fourth and so on). Initial position Invert Membrane Number of points Phase Reflect Rope Save frames as png Shift Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Total number of points: Vertical Visit GVB website. Waveform Wavelenght When working in two dimensions, points are arranged in a rectangular grid: if the chosen number doesn't fit, some points may be dropped. _Contents _Disposition _File _Help _Options calculation ms. cos (shifted) discontinuous peak drawing ms. flat gut half sin maximum minimum opposite sinusoidals opposite triangulars peak picked picked lateral pond product sin sinusoidal sinusoidal signal square square signal sum triangular signal waterfall Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2010-07-23 14:38+0000
Last-Translator: Serge Gavrilenko <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-07-24 03:54+0000
X-Generator: Launchpad (build Unknown)
 %d измерение: продвинутый %d измерение: подготовленный %d измерения: продвинутые %d измерения: подготовленные Симулятор волн. О программе Good ViBrations Добавить к текущему расположению Амплитуда Расчёт Центровка Выберите путь Правило сопоставления: Файл уже существует Кадров в секунду Good ViBrations - бесплатное программное обеспечение; вы можете распространять и/или изменять его на условиях лицензии GNU GPL, опубликованной Free Software Foundation;  используется версия 3 либо (по вашему выбору) более поздняя.

Good ViBrations распространяется с надеждой, что программы будет полезна, но БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ; даже без гарантии ТОВАРНОСТИ или ПРИГОДНОСТИ ДЛЯ КОНКРЕТНОЙ ЦЕЛИ. Смотрите лицензию GNU GPL для подробностей.

Вы должны были получить копию лицензии GNU GPL вместе с программой Good ViBrations; если это не так, свяжитесь с Free Software Foundation, Inc., 59 Temple Place - Офис 330, Бостон, MA 02111-1307 США. Графики Подсказка: Горизонтальный Если Вы хотите видеть гармоники, точное количество точек в размерности будет равняться n, поскольку n+1 имеет множество мелких факторов (например, если n+1 многократно содержит 2, як фактор, лучше будет рисовать вторую, четвертую, и т.д. гармонику). Начальное положение Инвертировать Мембрана Количество точек Фаза Отражение Верёвка Сохранить кадры как PNG Сдвиг Скорость Запустить Шаг Остановить Уже существует файл с таким-же именем,
вы уверены, что хотите перезаписать его? Полная длина Общее количество точек: Вертикальный Посетите сайт GVB. Форма волны Длина волны При работе в двух измерениях, точки располагаются в прямоугольной сетке: если выбранный номер не подходит, некоторые точки могут быть отброшены. _Содержание _Диспозиция _Файл _Справка _Параметры расчёт мс. косинус (сдвинутый) прерывистый рисование мс. плоский канал половина синуса максимум минимум противоположные синусоидальные противоположные треугольные пик выбранный выбранный боковой басейн продукт синус синусоидальный синусоидальный сигнал квадрат прямоугольный сигнал сумма треугольный сигнал водопад 