��    F      L  a   |                      0     H     a     t     �  	   �     �  	   �     �     �     �     �  �  �     �	     �	  
   �	  �   �	     �
     �
     �
     �
     �
     �
                          &     ,     1  N   6     �     �     �     �     �  
   �  �   �  	   c     m     z     �     �     �     �     �     �     �     �     �     �     �     �                    $     3     8     @  
   D     O     a     h     v     z  	   �  �  �     <     T     o     �     �     �     �     �     �                    4     I  �  c  	   7     A  
   J  �   U     N     `     i     r     �     �     �     �  	   �  	   �     �     �     �  I   �     &     5     N     W     m     {  �   �     5     A     O     X  	   _     i     w     �     �     �     �  
   �     �     �     �     �     �     �                       
   $     /     A     J     Z     _     q                .                #                      E       +   =   -   (                :          8                  >       3   7          '   2      !   C          "       0      &   )   @       5   /       ,             <   $         D                  %   *                 	   ;   4       9          A       F   
         6       B   ?   1            %d dimension: advanced %d dimension: precooked %d dimensions: advanced %d dimensions: precooked A waves simulator. About Good ViBrations Add to current disposition Amplitude Calculation Centering Choose a path Composition rule: File exists Frames/second Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Graphics Hint: Horizontal If you want to observe harmonics, the perfect number of points per dimension is an n such that n+1 has a lot of small factors (e.g: if n+1 contains many times 2 as factor, it will be possible to draw better the second harmonic, the  fourth and so on). Initial position Invert Membrane Number of points Phase Reflect Rope Save frames as png Shift Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Total number of points: Vertical Visit GVB website. Waveform Wavelenght When working in two dimensions, points are arranged in a rectangular grid: if the chosen number doesn't fit, some points may be dropped. _Contents _Disposition _File _Help _Options calculation ms. cos (shifted) discontinuous peak drawing ms. flat gut half sin maximum minimum opposite sinusoidals opposite triangulars peak picked picked lateral pond product sin sinusoidal sinusoidal signal square square signal sum triangular signal waterfall Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2010-07-02 09:43+0000
Last-Translator: Sergiy Gavrylov <sergiovana@bigmir.net>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-07-03 03:43+0000
X-Generator: Launchpad (build Unknown)
 %d dimensión: avanzada %d dimensión: precocinada %d dimensiones: avanzada %d dimensiones: precocinada Un simulador de ondas. Acerca de Good ViBrations Añadir a disposición actual Amplitud Cálculo Centrado Elija una ruta Regla de composición: El archivo ya existe Cuadros por segundo (fps) Good ViBrations es software libre; puede distribuirlo y/o modificarlo bajo los términos de la licencia GPL GNU publicada por la Fundación de Software Libre; ya sea la versión 3 de la licencia, o (a su criterio) cualquier versión posterior.

Good ViBrations se distribuye con la idea de que sea útil, pero SIN GARANTÍA ALGUNA; incluso sin la garantía implícita de MERCANTIBILIDAD ni de ADECUACIÓN A PROPÓSITO CONCRETO. Lea la licencia GPL GNU para conocer más detalles.

Debería haber recibido una copia de la licencia GPL GNU junto a este programa. En caso contrario, contacte con la Fundación en la siguiente dirección: Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Gráficos Consejo: Horizontal Si desea observar armónicos, el número ideal de puntos por dimensión es un entero n tal que n+1 posea muchos factores pequeños (p. e.: si n+1 contiene el factor 2 muchas veces, será posible dibujar mejor el armónico segundo, el cuarto, etc.). Posición inicial Invertir Membrana Número de puntos Fase Voltear Cuerda Guardar cuadro como png Desplazar Velocidad Comenzar Avance Detener Ya existe un archivo con este nombre,
¿seguro que desea sobreescribirlo? Longitud total Número total de puntos: Vertical Visitar GVB en la web Forma de onda Longitud de onda Al trabajar en dos dimensiones, los puntos se organizan en una rejilla rectangular: si el número elegido no se ajusta completamente, se perderán los puntos sobrantes. _Contenidos _Disposición _Archivo A_yuda _Opciones cálculo (ms) coseno (alzado) pico discontinuo dibujo (ms) plano tubo medio seno máximo mínimo sinusoidales opuestas triangulares opuestas pico pico pico lateral estanque producto seno sinusoidal señal sinusoidal cuadrada señal cuadrada suma señal triangular cascada 