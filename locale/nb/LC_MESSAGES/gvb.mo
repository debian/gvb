��    4      �  G   \      x     y     �  	   �     �  	   �     �     �     �  �  �     �     �  
   �     �     �     �     �     �     �     �          
            N        i     v     �     �     �  
   �  	   �     �     �     �     �     �     �     	     	     	     	     $	     9	     A	  
   E	     P	     b	     i	     w	     {	  	   �	  �  �	     ?     R  	   e  	   o  
   y     �     �     �  �  �     J     R     X     a     q     z     �     �  	   �     �  	   �     �     �     �  T   �     #     0     G     P  
   e     p     }     �     �     �     �     �  
   �     �  
   �     �     �     �     �                    *     2     A     E     T                             '                        3      +           $   ,                             
   /                   %   .   2   (             1              "   !           &   0         #   )       *   	   -      4                            A waves simulator. About Good ViBrations Amplitude Calculation Centering Choose a path File exists Frames/second Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Graphics Hint: Horizontal Initial position Invert Membrane Number of points Phase Reflect Save frames as png Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Total number of points: Vertical Visit GVB website. Waveform Wavelenght _Contents _Disposition _File _Help _Options calculation ms. drawing ms. flat half sin maximum minimum opposite sinusoidals product sin sinusoidal sinusoidal signal square square signal sum triangular signal waterfall Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2010-01-19 19:37+0000
Last-Translator: Tor Syversen <sol-moe@online.no>
Language-Team: Norwegian Bokmal <nb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-02-15 04:46+0000
X-Generator: Launchpad (build Unknown)
 En bølgesimulator Om Good ViBrations Amplitude Beregning Sentrering Velg en bane Filen eksisterer Bilder/sekund Good ViBrations er fri mykvare; du kan redistribuere det og/eller modifisere det under vilkårene i GNU General Public Licence publisert av Free Software Foundation; enten versjon 3 av lisensen, eller (etter eget valg) en senere versjon.

Good ViBrations er disribuert i håp om at det er nyttig. men det kommer UTEN NOEN GARANTI; uten engang en underforstått garanti om SALGBARHET eller DUGELIGHET FOR EN GITT HENSIKT. Se GNU General Public License for flere detaljer.

Du burde ha fått en kopi av GNU General Public License sammen med Good ViBrations; hvis ikke, skriv til Free Software Foundation, Inc., 39 Temple place - Suite 330, Boston, MA 02111-1307, USA. Grafikk Hint: Vannrett Utgangsposisjon Inverter Membran Antall poeng Fase Reflekter Lagre bilder som png Hastighet Start Steg Stopp Det finnes allerede en fil med dette navnet
er du sikker på at du vil erstatte den? Total lengde Totalt antall piksler: Loddrett Besøk GVB web side. Bølgeform Bølgelengde _Innhold _disposisjon _Fil _Hjelp _Alternativer kalkulasjon ms. tegner ms. flat halv sinus maksimum minimum motsatt sinusformet produkt sinus sinusformet sinusformet signal firkant firkant signal sum trekant signal foss 