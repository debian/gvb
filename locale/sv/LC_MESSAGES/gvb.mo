��    5      �  G   l      �     �     �     �     �  	   �     �  	   �               !     -  �  ;     �     �  
   �                    #     4     :     B     G     Z     `     f     l     q  N   v     �     �     �  
   �     �     �     	     	     	     !	     -	     2	     :	     B	     I	     N	     V	  
   Z	     e	     w	     ~	     �	  	   �	  �  �	     E     ]     v     �     �  
   �  
   �     �     �     �     �  �  �     �     �     �     �  	   �     �     �        
                  (  	   .     8     ?     D  V   K     �     �     �  
   �     �     �     �     �     �                         %     *     /     7     ;     G     Z     b     h  
   {                             )         	      -      !   4      .   ,       &                                    1                  '   (   3                         2       $   #          
      "      %   *      +      /      5       0                     %d dimension: advanced %d dimensions: advanced A waves simulator. About Good ViBrations Amplitude Calculation Centering Choose a path Composition rule: File exists Frames/second Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Graphics Hint: Horizontal Initial position Invert Membrane Number of points Phase Reflect Rope Save frames as png Shift Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Vertical Waveform Wavelenght _Disposition _File _Help _Options calculation ms. drawing ms. flat maximum minimum picked pond product sin sinusoidal sinusoidal signal square sum triangular signal waterfall Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2010-05-26 21:48+0000
Last-Translator: Jerry Bramstång <gloch_ftw@hotmail.com>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-28 03:38+0000
X-Generator: Launchpad (build Unknown)
 %d dimension: avancerad %d dimensions: avancerad En vågsimulator. Om Good ViBrations Amplitud Beräkning Centrering Välj en sökväg Kompositionsregel: Filen finns Bilder/sekund Good ViBrations är fri och öppen mjukvara; du kan redistribuera den och/eller modifiera den enligt villkoren i GNU General Public License som publicerade avFree Software Foundation; antingen version 3 av Licensen, eller (om du föredrar det) någon senare version.Good ViBrations har distribuerats med förhoppningen att vara användbar, men UTAN NÅGON GARANTI; till och med utan underförstådd garanti för SÄLJBARHET eller LÄMPLIGHET FÖR SITT SYFTE. Se GNU General Public License för fler detaljer. Du ska ha fått en kopia av GNU General Public License tillsammans med Good ViBrations; om intet, skriv till Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Grafik Tips: Vågrät Startposition Invertera Membran Antal punkter Fas Reflektera Rep Spara bilder som png Skift Hastighet Starta Steg Stoppa Det finns redan en fil med detta namn,
är du säker på att du vill skriva över den? Total längd Lodrät Vågform Våglängd _Disposition _Arkiv _Hjälp A_lternativ kalkyleringsmiss ritningsmiss rak maximum minimum vald damm produkt sin Sinusformad sinusformad signal kvadrat summa triangulär signal vattenfall 