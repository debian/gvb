��    ?        Y         p     q     �     �     �     �     �     �  	          	   +     5     C     U     a     o     x  
   ~  �   �     �     �     �     �     �     �     �     �     �     �     �     �     �  N   �     H     U     m     v     �  
   �  �   �  	   &	     0	     6	     <	     E	     U	     c	     v	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	  	   

  �  
     �     �     �     �          $  "   :  	   ]  
   g     r     ~     �     �     �     �     �  
   �    �     �     	               /     5     B     G     `     m     }     �     �  f   �     �               (  
   >     I  �   V     �     �                    &     7     L     Z     `     h     p     �     �     �     �     �     �     �     �     �     �  
   �                    2   :       >      	                  ,   ?   %   7          .             #   "   *   &                    =         )                             4                   $       
   3   '       !             5   0   (      +             6          9      <   ;      -   1          8               /              %d dimension: advanced %d dimension: precooked %d dimensions: advanced %d dimensions: precooked A waves simulator. About Good ViBrations Add to current disposition Amplitude Calculation Centering Choose a path Composition rule: File exists Frames/second Graphics Hint: Horizontal If you want to observe harmonics, the perfect number of points per dimension is an n such that n+1 has a lot of small factors (e.g: if n+1 contains many times 2 as factor, it will be possible to draw better the second harmonic, the  fourth and so on). Initial position Invert Membrane Number of points Phase Reflect Rope Save frames as png Shift Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Total number of points: Vertical Visit GVB website. Waveform Wavelenght When working in two dimensions, points are arranged in a rectangular grid: if the chosen number doesn't fit, some points may be dropped. _Contents _File _Help _Options calculation ms. cos (shifted) discontinuous peak drawing ms. flat maximum minimum opposite triangulars peak pond product sin sinusoidal sinusoidal signal square square signal sum triangular signal waterfall Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2010-09-07 13:20+0000
Last-Translator: Achim Behrens <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-08 04:38+0000
X-Generator: Launchpad (build Unknown)
 %d Dimension: erweitert %d Dimension: vorgekocht %d Dimensionen: erweitert %d Dimensionen: vorgekocht Ein Wellensimulator. Über Good ViBrations Zur aktuellen Disposition addieren Amplitude Berechnung Zentrierung Einen Pfad wählen Kompositions Regel: Datei existiert bereits Frames/Sekunde Grafik Hinweis: Horizontal Wenn sie Harmonische beobachten wollen, dann ist perfekte Anzahl der Punkte pro Dimension ein n, so dass n+1 sehr viele kleine Faktoren besitzt (z.B.: enthält n+1mehrmals den Faktor 2, dann wird es besser möglich sein, die Zweite, Vierte, usw. Harmonische zu zeichnen). Ursprungsposition Invertieren Membran Anzahl der Punkte Phase Reflektieren Seil Frames als png speichern Verschiebung Geschwindigkeit Start Schritt Stopp Es existiert bereits eine Datei mit diesem Namen.
Sind Sie sicher, dass Sie sie überschreiben wollen? Gesamtlänge Totale Anzahl der Punkte: Vertikal GVB Website besuchen. Wellenform Wellenlänge Im zweidimensionalen Modus sind die Punkte in einem rechteckigen Gitter angeordnet: wenn die gewählte Anzahl nicht passt, können einige Punkte verfallen. _Inhalt _Datei _Hilfe _Einstellungen Berechnung ms. cos (verschoben) unterbrochene Spitze Zeichnung ms. flach Maximum Minimum gegenüberliegende Dreiecke Spitze Teich Produkt Sin sinusförmig sinusförmiges Signal Reckteck Rechtecksignal Summe Dreiecksignal Wasserfall 