��    3      �  G   L      h     i     �     �     �     �     �  	   �     �  	                   ,     :     C  
   I     T     e     l     u     �     �     �     �     �     �     �     �     �  N   �          $     <     E     X  
   a  �   l  	   �     �                         !     )     .     6     :     A     O  	   S  �  ]     �     	     	     9	     T	     h	  	   {	  	   �	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  	   
     
     
     (
  	   .
     8
     >
     C
  P   H
     �
     �
     �
     �
  
   �
     �
  �   �
     �     �     �     �     �     �     �     �     �     �  
   �     �     �     �                            *               "      #          .   &   /   )           	                                                -   2       !          1              '              0      $   
   (   +      ,      %      3                             %d dimension: advanced %d dimension: precooked %d dimensions: advanced %d dimensions: precooked A waves simulator. About Good ViBrations Amplitude Calculation Centering Choose a path File exists Frames/second Graphics Hint: Horizontal Initial position Invert Membrane Number of points Phase Reflect Rope Save frames as png Shift Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Total number of points: Vertical Visit GVB website. Waveform Wavelenght When working in two dimensions, points are arranged in a rectangular grid: if the chosen number doesn't fit, some points may be dropped. _Contents _File _Help _Options flat maximum minimum peak product sin square square signal sum waterfall Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2010-06-18 21:26+0000
Last-Translator: Joe Hansen <Unknown>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-06-19 03:46+0000
X-Generator: Launchpad (build Unknown)
 %d dimension: Avanceret %d dimension: Behandlet %d dimensioner: Avanceret %d dimensioner: Behandlede En bølgesimulator. Om Good ViBrations Amplitude Beregning Centrering Vælg en sti Filen findes Rammer/sekund Grafik Råd: Vandret Udgansposition Modsat Membran Antal points Fase Reflekter Tov Gem rammer som png Skift Hastighed Start Trin Stop Der er allerede en fil med det navn,
er du sikker på, at du vil overskrive den? Total længde Totalt antal point: Lodret Besøg GVB's hjemmeside Bølgeform Bølgelængde Når der arbejdes i to dimensioner arrangeres punkter i et rektangulært net: Hvis det valgte tal ikke passer, vil nogle punkter måske blive tabt. _Indhold _Fil _Hjælp _Indstillinger flad maksimum minimum top produkt sin kvadratisk kvadratisk signal sum vandfald 