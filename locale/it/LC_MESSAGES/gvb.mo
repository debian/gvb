��    F      L  a   |                      0     H     a     t     �  	   �     �  	   �     �     �     �     �  �  �     �	     �	  
   �	  �   �	     �
     �
     �
     �
     �
     �
                          &     ,     1  N   6     �     �     �     �     �  
   �  �   �  	   c     m     z     �     �     �     �     �     �     �     �     �     �     �     �                    $     3     8     @  
   D     O     a     h     v     z  	   �  �  �     (     @     X     o     �     �  !   �     �     �  
                  7     F  j  Y     �     �     �  �   �     �     �  
                  "     +     1     M  	   Y     c     i     o  I   u     �     �  	   �     �            �   *  	   �     �     �     �     �     �               &     5     <  
   C     N     V     ]     q     �  	   �     �     �     �     �     �     �     �     �     �     �                     .                #                      E       +   =   -   (                :          8                  >       3   7          '   2      !   C          "       0      &   )   @       5   /       ,             <   $         D                  %   *                 	   ;   4       9          A       F   
         6       B   ?   1            %d dimension: advanced %d dimension: precooked %d dimensions: advanced %d dimensions: precooked A waves simulator. About Good ViBrations Add to current disposition Amplitude Calculation Centering Choose a path Composition rule: File exists Frames/second Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Graphics Hint: Horizontal If you want to observe harmonics, the perfect number of points per dimension is an n such that n+1 has a lot of small factors (e.g: if n+1 contains many times 2 as factor, it will be possible to draw better the second harmonic, the  fourth and so on). Initial position Invert Membrane Number of points Phase Reflect Rope Save frames as png Shift Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Total number of points: Vertical Visit GVB website. Waveform Wavelenght When working in two dimensions, points are arranged in a rectangular grid: if the chosen number doesn't fit, some points may be dropped. _Contents _Disposition _File _Help _Options calculation ms. cos (shifted) discontinuous peak drawing ms. flat gut half sin maximum minimum opposite sinusoidals opposite triangulars peak picked picked lateral pond product sin sinusoidal sinusoidal signal square square signal sum triangular signal waterfall Project-Id-Version: Gnome ViBrations 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2010-07-02 09:59+0000
Last-Translator: Sergiy Gavrylov <sergiovana@bigmir.net>
Language-Team: Italian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-07-03 03:43+0000
X-Generator: Launchpad (build Unknown)
 %d dimensione: avanzato %d dimensione: precotte %d dimensioni:avanzato %d dimensioni: precotte Un simulatore di vibrazioni. Informazioni su Good ViBrations Aggiungere alla posizione attuale Ampiezza Metodo di calcolo Centratura Scegli un percorso Regola di composizione: File esistente Fotogrammi/secondo Good ViBrations è software libero; puoi redistribuirlo e/o modificarlo nei termini della GNU General Public License come pubblicato dalla Free Software Foundation versione 2 o successive.

Good ViBrations è distribuito sperando sia utile, ma SENZA ALCUNA GARANZIA espressa o implicita, di COMMERCIABILITÀ o di IDONEITÀ AD UNO SCOPO PARTICOLARE. Vedere la GNU General Public License per ulteriori dettagli.

Dovresti aver ricevuto una copia della GNU General Public License con Good ViBrations; se così non fosse, scrivi alla Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Grafica Suggerimento: Orizzontale Se si vuole studiare le armoniche, il numero ideale di punti per dimensione è un n tale che n+1 abbia molti fattori piccoli (ad esempio, se n+1 contiene molte volte il 2 come fattore, sarà possibile disegnare al meglio le armoniche seconda, quarta...) Disposizione iniziale Inverti Superficie Numero di punti Fase Rifletti Corda Salva i fotogrammi come png Spostamento Velocità Parti Passo Ferma Esiste già un file con questo nome,
sei sicuro di volerlo sovrascrivere? Lunghezza totale Numero totale di punti: Verticale Visita il sito web di GVB Forma d'onda Lunghezza d'onda Quando si lavora in due dimensioni, i punti sono disposti in una griglia rettangolare: se il numero scelto non è adatto potranno esserne utilizzati meno. _Sommario _Disposizioni _File _Aiuto _Opzioni ms. di calcolo coseno (traslato) picco discontinuo ms. di disegno piatta goccia mezzo seno massimo minimo sinusoidali opposti triangolari opposti picco pizzicata pizzicata lateralmente stagno prodotto seno sinusoidale segnale sinusoidale quadra segnale quadro somma segnale triangolare cascata 