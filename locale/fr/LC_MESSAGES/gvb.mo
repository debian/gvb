��    F      L  a   |                      0     H     a     t     �  	   �     �  	   �     �     �     �     �  �  �     �	     �	  
   �	  �   �	     �
     �
     �
     �
     �
     �
                          &     ,     1  N   6     �     �     �     �     �  
   �  �   �  	   c     m     z     �     �     �     �     �     �     �     �     �     �     �     �                    $     3     8     @  
   D     O     a     h     v     z  	   �  �  �     7     N     i     �     �     �  "   �  	   �     �               !     7     I  w  Y  	   �     �  
   �  )  �          +     4     =     N     T     [     a     �     �  	   �     �     �  R   �     �          &     /     J     Z  �   j               %     .     4  
   =     H     W  
   f     q     x          �     �     �     �     �     �     �     �     �     �     �               !     /     5     I                .                #                      E       +   =   -   (                :          8                  >       3   7          '   2      !   C          "       0      &   )   @       5   /       ,             <   $         D                  %   *                 	   ;   4       9          A       F   
         6       B   ?   1            %d dimension: advanced %d dimension: precooked %d dimensions: advanced %d dimensions: precooked A waves simulator. About Good ViBrations Add to current disposition Amplitude Calculation Centering Choose a path Composition rule: File exists Frames/second Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Graphics Hint: Horizontal If you want to observe harmonics, the perfect number of points per dimension is an n such that n+1 has a lot of small factors (e.g: if n+1 contains many times 2 as factor, it will be possible to draw better the second harmonic, the  fourth and so on). Initial position Invert Membrane Number of points Phase Reflect Rope Save frames as png Shift Speed Start Step Stop There is already a file with this name,
are you sure you want to overwrite it? Total lenght Total number of points: Vertical Visit GVB website. Waveform Wavelenght When working in two dimensions, points are arranged in a rectangular grid: if the chosen number doesn't fit, some points may be dropped. _Contents _Disposition _File _Help _Options calculation ms. cos (shifted) discontinuous peak drawing ms. flat gut half sin maximum minimum opposite sinusoidals opposite triangulars peak picked picked lateral pond product sin sinusoidal sinusoidal signal square square signal sum triangular signal waterfall Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2012-02-08 08:57+0000
Last-Translator: Messer Kevin <messer.kevin@gmail.com>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2012-02-09 05:23+0000
X-Generator: Launchpad (build 14763)
 %d dimension : avancé %d dimension: prédéfinie %d dimensions : avancé %d dimensions: prédéfinies Un simulateur d'ondes. À Propos de Good ViBrations Ajouter à la disposition actuelle Amplitude Calcul Centrage Choisir un chemin Règle de composition Le fichier existe Frames/secondes Good ViBrations est un logiciel libre : vous pouvez le redistribuer et/ou le modifier selon les termes de la GNU/GPL publiée par la Free Software Foundation ; soit en version 3 de cette licence, soit supérieure si vous voulez. 

Good ViBrations est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, sans même la garntie d'une VALERU COMMERCIALE ou APTITUDES DANS UN BUT PARTICULIER. Voir la GNU/GPL pour plus d'informations.

Vous devriez avoir reçu une copie de la GNU/GPL avec Good ViBrations, si non, écrivez à la Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Affichage Conseil: Horizontal Si vous voulez observer les harmoniques, le nombre idéal de points par dimension est un n tel que n +1 a beaucoup de petits facteurs (par exemple: si n +1 contient de nombreuses fois 2 en tant que facteur, il sera possible de mieux dessiner le second harmonique, le quatrième et ainsi de suite). Position initiale Inverser Membrane Nombre de points Phase Reflet Corde Sauvegarder les images en png. Déplacement Vitesse Commencer Pas Arrêter Il existe déjà un fichier avec ce nom.
Voulez vous vraiment écraser ce fichier? Longueur totale Nombre total de points Vertical Visiter le site web de GVB Forme de l'onde Longueur d'onde En travaillant en deux dimensions, Les points sont disposés dans une grille rectangulaire : Si le nombre choisi n'est pas convenable, certains points peuvent perdu. Teneurs _Disposition _Fichier _Aide _Options calcul ms. cos (décalé) pic discontinu dessin ms. aplati goutte péché à moitié maximum minimum sinusoidals opposées triangulaires opposés pic choix choix latéral étang produit sin sinusoïdal Signal sinusoïdal carré Signal carré somme Signal triangulaire cascade 