��    +      t  ;   �      �     �     �  	   �     �  	   �               "     .  �  <     �     �  
   �                    $     5     ;     C     H     [     a     g     m     r     w     �     �  
   �     �     �     �     �     �     �     �     �     �     �  
   �     �  �       �	     �	  	   �	  
   �	  	   �	     �	     �	     �	     
  �  
     �     �     �     �     �     �     �                    !     8     >     G     M     R     Z  	   g     q  
   z     �     �     �     �     �     �     �     �     �     �     �     �                    
         "      %                          (                                      	   #          +                     *            '                      )          &   !         $        A waves simulator. About Good ViBrations Amplitude Calculation Centering Choose a path Composition rule: File exists Frames/second Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Graphics Hint: Horizontal Initial position Invert Membrane Number of points Phase Reflect Rope Save frames as png Shift Speed Start Step Stop Total lenght Vertical Waveform Wavelenght _File _Help _Options calculation ms. drawing ms. flat maximum minimum product sin sinusoidal sum Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2011-03-23 19:22+0000
Last-Translator: Ewout De cat <Unknown>
Language-Team: Dutch <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2011-03-24 04:47+0000
X-Generator: Launchpad (build 12559)
 Een golfsimulator. Over Good ViBrations Amplitude Berekening Centreren Kies een pad Compositieregel: Bestand bestaat al Frames/seconde Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Beeld Hint: Horizontaal Beginpositie Omkeren Membraan Aantal punten Fase Reflecteren Touw Frames opslaan als PNG Shift Snelheid Start Stap Stoppen Totaallengte Verticaal Golfvorm Golflengte _Bestand _Help _Opties berekening ms. tekenen ms. vlak maximum minimum product zonde sinusoidaal som 