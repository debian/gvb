��            )   �      �     �     �     �     �     �               &  �  4     �     �     �                    -     3     9     >     K     Q     W     `     p     |     �     �     �  �  �     4     L     e     ~     �     �  	   �     �  �  �     �     �  	   �     �     �     �                         )     1     =     D     S     g     o     w                                                                   	         
                                                                   %d dimension: advanced %d dimension: precooked %d dimensions: advanced %d dimensions: precooked A waves simulator. About Good ViBrations Calculation Frames/second Good ViBrations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Good ViBrations is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Good ViBrations; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Graphics Initial position Membrane Number of points Rope Save frames as png Speed Start Step _Disposition _File _Help _Options calculation ms. drawing ms. flat maximum minimum product Project-Id-Version: gvb
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-14 16:36+0100
PO-Revision-Date: 2010-09-16 19:22+0000
Last-Translator: Konki <pavel.konkol@seznam.cz>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-17 04:48+0000
X-Generator: Launchpad (build Unknown)
 %d rozměr: pokročilé %d rozměr: předvařeno %d rozměry: pokročilé %d rozměry: předvařeno Simulátor vlnění. O Good ViBrations Výpočet Snímků za sekundu Good ViBrations je svobodný software; můžete jej šířit a modifikovat podle ustanovení GNU General Public License, vydávané Free Software Foundation; a to buď verze 3 této licence anebo (podle svého uvážení) kterékoli pozdější verze.

Program Good ViBrations je rozšiřován v naději, že bude užitečný, avšak BEZ JAKÉKOLI ZÁRUKY; neposkytují se ani odvozené záruky PRODEJNOSTI anebo VHODNOSTI PRO URČITÝ ÚČEL. Další podrobnosti hledejte v GNU General Public License.

Kopii GNU General Public License jste měli obdržet spolu s programem Good ViBrations; pokud se tak nestalo, napište na adresu Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA Grafika Výchozí pozice Membrána Počet bodů Křivka Uložit ve formátu PNG Rychlost Spustit Krok Uspořá_dání _Soubor Nápo_věda V_olby výpočet [ms] vykreslování [ms] ploché maximum minimum produkt 