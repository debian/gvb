Source: gvb
Section: science
Priority: optional
Maintainer: Pietro Battiston <me@pietrobattiston.it>
Build-Depends: debhelper (>= 9), dh-python
Build-Depends-Indep: python3, python3-setuptools, python3-distutils-extra
Standards-Version: 3.9.5
Homepage: http://www.pietrobattiston.it/gvb
Vcs-Browser: https://salsa.debian.org/debian/gvb
Vcs-Git: https://salsa.debian.org/debian/gvb.git

Package: gvb
Architecture: all
Depends: ${misc:Depends}, python3, python3-scipy, python-gi, python3-gi-cairo
Suggests: libav-tools
Description: visual simulator of 1 and 2-dimensional vibrations
 Good ViBrations (gvb) is a small program that aims at providing a nice
 interface to play with waves in 1 or 2 dimensions.
 .
 It features several ways of setting initial conditions, as well as different
 calculation methods and graphic outputs. It is also possible to dump animation
 frames to png images in order to make a movie with them.
 .
 It relies on the Python library scipy to get the best possible performance in
 calculations.
